# node-standardpaths

This project is an attempt to bring [QStandardPaths](http://doc.qt.io/qt-5/qstandardpaths.html) from the Qt Project to JavaScript and Node to get common platform-specific standard paths.

This includes paths like
 - the **configuration directories** used to store system and user configuration files,
 - **data directories**,
 - **cache directories**,
 - and several others. (see the QStandardPaths documentation for the planned paths)

Targeted platforms are all which Node supports, which are
 - Linux, FreeBSD and Solaris [*XDG Base Directory Specification*]
 - Darwin (Mac OS X, macOS)
 - Windows


#### Supported Paths

At the moment this library can report the following paths.

| Type                    | Description                                         |
| ----------------------- | --------------------------------------------------- |
| HomePath                | Users home path                                     |
| ConfigLocation          | Users configuration directory                       |
| SystemConfigLocation    | Global configuration directory                      |


### Unresolved Paths

Besides of getting the mentioned paths above, this library has the possibility to report any of the above paths in unresolved form, which means instead of a valid path you get the path with environment variables (for example `$HOME` instead of `/home/user`). The unresolved paths will never contain non-existent environment variables (for example if `$XDG_CONFIG_HOME` isn't set or empty, `$HOME/.config` will be returned instead).


## Usage


Example code snippet
```js
var standardpaths = require('node-standardpaths');

standardpaths.path(standardpaths.HomePath);
standardpaths.path(standardpaths.HomePath, false);

// returns the following
// on Linux, FreeBSD, Solaris: '/home/user', '$HOME'
// on Darwin:                  '/Users/user', '$HOME'
// on Windows:                 'C:\Users\user' '%USERPROFILE%'
```

**ATTENTION:** the API is in the progress of being changed, please be aware of that.

Something unclear? Join my community [Discord](https://discord.gg/4dpCQXv) server and get further assistance in `#node-standardpaths`.
